var cluster = require('cluster');
    var net =  require('net');
    var userList = [];
    var server = net.createServer(function(socket){
        var user = {name:socket.remoteAddress+":"+socket.remotePort,socket:socket};
        userList.push(user);
        broadCast(user,`${user.name} has comming`);

        socket.on('data',buf=>{
            console.log(buf.toString());
        })

        socket.on('end',()=>{
            broadCast(user,`${socket.remoteAddress} has end`)
            console.log(`${socket.remoteAddress} has end`)
            userList.splice(userList.indexOf(user),1);
        })
    });
    server.listen(8124,'0.0.0.0',()=>{
        console.log('listening:',server.address())
    })

    function broadCast(u,message){
        var cleanList = []
        userList.forEach(user=>{
            if(user !== u){
                if(user.socket.readable){
                    user.socket.write(message)
                }else{
                    cleanList.push(user.socket)
                }
            }
        })
        cleanList.forEach(client=>{
            userList.splice(userList.indexOf(client),1)
        })
    }
