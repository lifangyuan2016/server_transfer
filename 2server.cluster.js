var net = require('net');
var cluster = require('cluster');
var CPUnums = 2;
var userList = [];

if(cluster.isMaster){
    for(var i=0;i<CPUnums;i++){
        cluster.fork();
    }
    var server = net.createServer({pauseOnConnect:true},function(socket){
        userList.push(socket)
        setTimeout(()=>socket.write('sdfsdfsdf'),2000)
        cluster.workers[Math.trunc(Math.random()*CPUnums)+1].send('socket',socket);
    })
    server.listen(8124,'0.0.0.0',function(){
        console.log('Main process start serving');
    });
    cluster.on('message',function(worker,msg){
        if(msg.type === 'broadCast'){
            broadCast(msg.content);
        }
    })
}else{
    console.log(`Start PROCESS:${cluster.worker.id}`)
    process.on('message',function(msg,socket){
        console.log(`${socket.remoteAddress}:${socket.remotePort} joined PROCESS:${cluster.worker.id}`)
        socket.on('data',function(data){
            socket.write('Echo data:'+data);
            process.send({type:'broadCast',content:data.toString()});
        })
    })
}
function broadCast(msg){
    userList = userList.filter(function(socket){
        return socket.writable
    })
    console.log(userList.length);
    userList.forEach(function(socket){
        socket.write(msg)
    })
}
