var cluster = require('cluster');
var net =  require('net');
var userList = [];
var numCPUs = 2

const log4js = require('log4js');
log4js.configure({
    appenders: { cheese: { type: 'file', filename: 'cheese.log' } },
    categories: { default: { appenders: ['cheese'], level: 'trace' } }
});
const logger = log4js.getLogger('tcp');

if(cluster.isMaster){
    for(var i=0;i<numCPUs;i++){
        cluster.fork();
    }

    for(var i in cluster.workers){
        cluster.workers[i].on('message',messageHandle);
    }
}else{
    var server = net.createServer({pauseOnConnect:true},function(socket){
        logger.info('Join process:',cluster.worker.id);

        process.send({type:'joinList',content:""},socket)
    });
    server.listen(8124,'0.0.0.0',()=>{
        logger.info('listening:',server.address())
    })

}
function messageHandle(msg,socket){
    switch(msg.type){
        case 'joinList':
            var userName = socket.remoteAddress + socket.remotePort;
            userList.push(socket);
            broadCast(socket,`${userName} has comming`);

            socket.on('data',buf=>{
                logger.info(`PROCESS ${this.id}: MSG from:${userName} CONTENT:${buf.toString()}`);
            })
            socket.on('end',()=>{
                userList.splice(userList.indexOf(socket),1)
                broadCast(socket,`${userName} has end`)
                logger.info(`${userName} has left`)
                socket.destroy();
            })
            break;

        default:
            break;
    }
}
function broadCast(u,message){
    var cleanList = []
    userList.forEach(socket=>{
        if(socket !== u){
            if(socket.readable){
                socket.write(message)
            }else{
                cleanList.push(socket)
                socket.destroy()
            }
        }
    })
    cleanList.forEach(client=>{
        userList.splice(userList.indexOf(client),1)
    })
}
